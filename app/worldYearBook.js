'use strict'
const fs = require('fs');
const Encoding = require('encoding-japanese');
const {promisify} = require('util');
const conv = require('./convert');

/**
 * 変換処理
 * 
 * @param {*} fileName 
 */
function getText(fileName) {
    const buffer = fs.readFileSync('./storage/input/' + fileName);
    const text = Encoding.convert(buffer, {
        from: 'SJIS',
        to: 'UNICODE',
        type: 'string',
    });

    putFile(fileName, conv.convWorldYearBookText(text));
}

/**
 * 変換後コンテンツをファイル出力
 * 
 * @param {*} fileName 
 * @param {*} text 
 */
function putFile(fileName, text) {
    try {
        fs.writeFileSync('./storage/output/' + fileName, text);
    }catch(e){
        console.log(e);
    }
}

/**
 * ファイル一覧取得
 */
async function getFileList() {
    const fileList = await promisify(fs.readdir)('./storage/input').then(files => {
        return files.filter(file => /.*\.txt$/.test(file));
    });

    fileList.forEach(function(file) {
        console.log('start! ' + file);
        getText(file);
    });

    console.log('done!');
}

getFileList();
