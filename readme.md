## 概要
* convertToolで変換できるが、対象ファイルが多いため一括変換する仕組みを作った
* 既存の変換処理をnodeから呼び出す
* sjisだったので encoding-japanese を使用して変換

## 準備
* ./app/storage/input に対象のtextファイルを置く
* convert.js に更新がない確認。あればファイルを入れ替える

拡張子は.txtのみの想定で作成

### convert.jsの入れ替え

* 最新版で上書き
* fileの最後に以下を追加して保存

```js
exports.convWorldYearBookText = function (text) {
	return(convertText(text));
}
```

## 起動と実行
```
docker-compose up -d --build
docker exec -it world-year-book /bin/bash
yarn install
node worldYearBook.js
```

## memo
入力ファイルをもらった際、windowsでzip圧縮してファイル名がsjisになり文字化けした。圧縮せず共有フォルダを使用して回避